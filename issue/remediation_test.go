package issue

import (
	"reflect"
	"testing"
)

func TestNewRef(t *testing.T) {
	vuln := Issue{
		CompareKey: "debian:10:nghttp2:CVE-2019-9511",
	}

	want := Ref{
		ID:         "949fba59cbbf688d2a7633b5fb5ec6767d39cb44fb932ee4857b22efddf87f7d",
		CompareKey: "debian:10:nghttp2:CVE-2019-9511",
	}

	got := NewRef(vuln)
	if !reflect.DeepEqual(got, want) {
		t.Errorf("Wrong result. Expected:\n%#v\nBut got:\n%#v", want, got)
	}
}
