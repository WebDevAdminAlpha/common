package issue

import (
	"reflect"
	"testing"
)

func TestCurrentVersion(t *testing.T) {
	want := Version{Major: 3, Minor: 0, Patch: 0, PreRelease: ""}
	got := CurrentVersion()
	if !reflect.DeepEqual(want, got) {
		t.Errorf("Wrong result. Expecting %v but got %v", want, got)
	}
}

func TestVersion(t *testing.T) {
	t.Run("String", func(t *testing.T) {
		want := "12.34.56"
		got := Version{Major: 12, Minor: 34, Patch: 56}.String()
		if got != want {
			t.Errorf("Wrong result. Expecting %s but got %s", want, got)
		}
	})

	t.Run("String with PreRelease", func(t *testing.T) {
		want := "12.34.56-rc1"
		got := Version{Major: 12, Minor: 34, Patch: 56, PreRelease: "rc1"}.String()
		if got != want {
			t.Errorf("Wrong result. Expecting %s but got %s", want, got)
		}
	})

	t.Run("MarshalJSON", func(t *testing.T) {
		want := []byte(`"12.34.56"`)
		got, err := Version{Major: 12, Minor: 34, Patch: 56}.MarshalJSON()
		if err != nil {
			t.Fatal(err)
		}
		if string(got) != string(want) {
			t.Errorf("Wrong result. Expecting %s but got %s", string(want), string(got))
		}
	})

	t.Run("MarshalJSON with PreRelease", func(t *testing.T) {
		want := []byte(`"12.34.56-rc1"`)
		got, err := Version{Major: 12, Minor: 34, Patch: 56, PreRelease: "rc1"}.MarshalJSON()
		if err != nil {
			t.Fatal(err)
		}
		if string(got) != string(want) {
			t.Errorf("Wrong result. Expecting %s but got %s", string(want), string(got))
		}
	})

	t.Run("UnarshalJSON", func(t *testing.T) {
		var tcs = []struct {
			Name  string
			Input string
			Want  Version
		}{
			{
				Name:  "Empty",
				Input: `null`,
				Want:  Version{Major: VersionMajor, Minor: VersionMinor},
			},
			{
				Name:  "Major",
				Input: `"12"`,
				Want:  Version{Major: 12},
			},
			{
				Name:  "Major.Minor",
				Input: `"12.34"`,
				Want:  Version{Major: 12, Minor: 34},
			},
			{
				Name:  "Major.Minor.Patch",
				Input: `"12.34.56"`,
				Want:  Version{Major: 12, Minor: 34, Patch: 56},
			},
			{
				Name:  "Major.Minor.Patch-Pre",
				Input: `"12.34.56-rc1"`,
				Want:  Version{Major: 12, Minor: 34, Patch: 56, PreRelease: "rc1"},
			},
		}

		for _, tc := range tcs {
			t.Run(tc.Name, func(t *testing.T) {
				var got Version
				err := got.UnmarshalJSON([]byte(tc.Input))
				if err != nil {
					t.Fatal(err)
				}
				if got != tc.Want {
					t.Errorf("Wrong result. Expecting %v but got %v", tc.Want, got)
				}
			})
		}
	})
}
