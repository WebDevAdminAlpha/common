package command

var errInvalidArgs = &ErrInvalidArgs{}

// ErrInvalidArgs is an error that occurs when
// any of the analyzer CLI commands receives
// unexpected arguments.
type ErrInvalidArgs struct{}

func (e ErrInvalidArgs) Error() string {
	return "Invalid arguments"
}

// ExitCode returns the analyzer CLI application
// exit code which should be returned upon analyzer
// termination when ErrInvalidArgs occurs.
func (e ErrInvalidArgs) ExitCode() int {
	return 2
}
